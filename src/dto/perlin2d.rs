pub struct Perlin2D {
    pub seed: u32,
    pub offset: (f64, f64, f64),
    pub scale: (f64, f64, f64),
    pub clamp: (f64, f64),
}
