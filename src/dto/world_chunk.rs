pub struct WorldChunkDTO {
    pub name: String,
    pub location: (isize, isize, isize),
}

impl WorldChunkDTO {
    pub fn new(name: String, location: (isize, isize, isize)) -> WorldChunkDTO {
        WorldChunkDTO {
            name: name,
            location: location,
        }
    }

    pub fn copy(&self) -> WorldChunkDTO {
        WorldChunkDTO {
            name: self.name.to_string(),
            location: self.location,
        }
    }
}
