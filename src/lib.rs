#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate savefile_derive;

use pyo3::prelude::*;
use pyo3::types::*;
use pyo3::wrap_pyfunction;

use std::sync::Mutex;
use std::time::Instant;

mod bge;
mod dto;
mod logic;

use crate::logic::maps::*;
use crate::logic::mesh::*;

const DEBUG: bool = false;

lazy_static! {
    static ref MAPS: Mutex<Maps> = Mutex::new(Maps::new());
}

#[pyfunction]
fn map_create(name: String, size_x: usize, size_y: usize, size_z: usize) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    maps.create(&name, size_x, size_y, size_z);

    if DEBUG {
        println!(
            "map_create(name:{}, size_x:{}, size_y:{}, size_z:{}) Time: {}",
            &name,
            &size_x,
            &size_y,
            &size_z,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_destroy(name: String) -> PyResult<bool> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    let result = maps.destroy(&name);

    if DEBUG {
        println!(
            "map_destroy(name:{}) Time: {}",
            &name,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(result)
}

#[pyfunction]
fn map_save(name: String) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    maps.save(&name);

    if DEBUG {
        println!(
            "map_save(name:{}) Time: {}",
            &name,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_load(name: String) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    maps.load(&name);

    if DEBUG {
        println!(
            "map_load(name:{}) Time: {}",
            &name,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_clear(name: String) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            map.clear();
        }
        None => {}
    }

    if DEBUG {
        println!(
            "map_clear(name:{}) Time: {}",
            &name,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_add_rough_sphere(name: String, x: f32, y: f32, z: f32, r: f32) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            map.add_rough_sphere(x, y, z, r);
        }
        None => {}
    }

    if DEBUG {
        println!(
            "map_add_rough_sphere(name:{}, x:{}, y:{}, z:{}, r:{}) Time: {}",
            &name,
            &x,
            &y,
            &z,
            &r,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_del_rough_sphere(name: String, x: f32, y: f32, z: f32, r: f32) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            map.del_rough_sphere(x, y, z, r);
        }
        None => {}
    }

    if DEBUG {
        println!(
            "map_del_rough_sphere(name:{}, x:{}, y:{}, z:{}, r:{}) Time: {}",
            &name,
            &x,
            &y,
            &z,
            &r,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_add_perfect_sphere(name: String, x: f32, y: f32, z: f32, r: f32) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            map.add_perfect_sphere(x, y, z, r);
        }
        None => {}
    }

    if DEBUG {
        println!(
            "map_add_perfect_sphere(name:{}, x:{}, y:{}, z:{}, r:{}) Time: {}",
            &name,
            &x,
            &y,
            &z,
            &r,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn map_del_perfect_sphere(name: String, x: f32, y: f32, z: f32, r: f32) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            map.del_perfect_sphere(x, y, z, r);
        }
        None => {}
    }

    if DEBUG {
        println!(
            "map_del_perfect_sphere(name:{}, x:{}, y:{}, z:{}, r:{}) Time: {}",
            &name,
            &x,
            &y,
            &z,
            &r,
            (now.elapsed().as_millis() as f64) / 1000_f64
        );
    }

    Ok(())
}

#[pyfunction]
fn blender_show_map(name: String) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            let gil = Python::acquire_gil();
            let py = gil.python();

            let cube = py
                .import("bpy")?
                .get("data")?
                .getattr("objects")?
                .get_item(&name)?;

            let data = cube.getattr("data")?;

            &data.call_method0("clear_geometry");

            let mut verts: Vec<(f32, f32, f32)> = Vec::new();
            let edges: Vec<(usize, usize)> = Vec::new();
            let faces: Vec<(usize, usize, usize)> = Vec::new();

            for x in 0..map.size_x {
                for y in 0..map.size_y {
                    for z in 0..map.size_z {
                        if map.data[x][y][z].is_full {
                            &verts.push((x as f32, y as f32, z as f32));
                        }
                    }
                }
            }

            let pyverts: &PyList = PyList::new(py, &verts);
            let pyedges: &PyList = PyList::new(py, &edges);
            let pyfaces: &PyList = PyList::new(py, &faces);

            &data.call_method1("from_pydata", (pyverts, pyedges, pyfaces));

            if DEBUG {
                println!(
                    "blender_show_map(name:{}) Time: {}",
                    &name,
                    (now.elapsed().as_millis() as f64) / 1000_f64
                );
            }
        }
        None => {}
    }

    Ok(())
}

#[pyfunction]
fn blender_update_mesh(name: String) -> PyResult<()> {
    let now = Instant::now();

    let mut maps = MAPS.lock().unwrap();
    match maps.get(&name) {
        Some(map) => {
            let gil = Python::acquire_gil();
            let py = gil.python();

            let cube = py
                .import("bpy")?
                .get("data")?
                .getattr("objects")?
                .get_item(&name)?;

            let data = cube.getattr("data")?;

            &data.call_method0("clear_geometry");

            let mesh = Mesh::new(&map);

            let verts: Vec<(f32, f32, f32)> = mesh.verts;
            let edges: Vec<(usize, usize)> = Vec::new();
            let faces: Vec<(usize, usize, usize)> = mesh.faces;

            let pyverts: &PyList = PyList::new(py, &verts);
            let pyedges: &PyList = PyList::new(py, &edges);
            let pyfaces: &PyList = PyList::new(py, &faces);

            &data.call_method1("from_pydata", (pyverts, pyedges, pyfaces));

            if DEBUG {
                println!(
                    "blender_update_mesh(name:{}) Time: {}",
                    &name,
                    (now.elapsed().as_millis() as f64) / 1000_f64
                );
            }
        }
        None => {}
    }
    Ok(())
}

#[pymodule]
fn thrust(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(map_create))?;
    m.add_wrapped(wrap_pyfunction!(map_destroy))?;
    m.add_wrapped(wrap_pyfunction!(map_save))?;
    m.add_wrapped(wrap_pyfunction!(map_load))?;
    m.add_wrapped(wrap_pyfunction!(map_clear))?;
    m.add_wrapped(wrap_pyfunction!(map_add_rough_sphere))?;
    m.add_wrapped(wrap_pyfunction!(map_del_rough_sphere))?;
    m.add_wrapped(wrap_pyfunction!(map_add_perfect_sphere))?;
    m.add_wrapped(wrap_pyfunction!(map_del_perfect_sphere))?;
    m.add_wrapped(wrap_pyfunction!(blender_show_map))?;
    m.add_wrapped(wrap_pyfunction!(blender_update_mesh))?;

    m.add_class::<bge::world2d::World2D>()?;
    Ok(())
}
