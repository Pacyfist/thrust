use std::collections::HashMap;

use savefile::prelude::*;

use crate::logic::map::*;

pub struct Maps {
    data: HashMap<String, Map>,
}

impl Maps {
    pub fn new() -> Maps {
        Maps {
            data: HashMap::new(),
        }
    }

    pub fn exists(&mut self, name: &String) -> bool {
        self.data.contains_key(name)
    }

    pub fn get(&mut self, name: &String) -> Option<&mut Map> {
        self.data.get_mut(name)
    }

    pub fn create(&mut self, name: &String, size_x: usize, size_y: usize, size_z: usize) {
        self.data
            .insert(name.clone(), Map::new(size_x, size_y, size_z));
    }

    pub fn destroy(&mut self, name: &String) -> bool {
        let mut result: bool = false;

        if self.data.contains_key(name) {
            self.data.remove(name);

            result = true;
        }

        result
    }

    pub fn save(&mut self, name: &String) {
        let map = self.get(&name).unwrap();
        save_file("save.bin", 0, map).unwrap();
    }

    pub fn load(&mut self, name: &String) {
        let map: Map = load_file("save.bin", 0).unwrap();
        self.data.insert(name.clone(), map);
    }
}
