#[derive(Savefile, Clone)]
pub struct Corner {
    pub is_full: bool,
    pub delta_x: u8,
    pub delta_y: u8,
    pub delta_z: u8,
}
