use crate::logic::corner::*;
use noise::{NoiseFn, Perlin, Seedable};

use crate::dto::perlin2d::Perlin2D;

#[derive(Savefile)]
pub struct Map {
    pub size_x: usize,
    pub size_y: usize,
    pub size_z: usize,
    pub data: Vec<Vec<Vec<Corner>>>,
}

impl Map {
    pub fn new(size_x: usize, size_y: usize, size_z: usize) -> Map {
        let data = vec![
            vec![
                vec![
                    Corner {
                        is_full: false,
                        delta_x: 0,
                        delta_y: 0,
                        delta_z: 0,
                    };
                    size_z + 1
                ];
                size_y + 1
            ];
            size_x + 1
        ];

        Map {
            size_x: size_x + 1,
            size_y: size_y + 1,
            size_z: size_z + 1,
            data: data,
        }
    }

    pub fn clear(&mut self) {
        for x in 0..self.size_x {
            for y in 0..self.size_y {
                for z in 0..self.size_z {
                    self.data[x][y][z] = Corner {
                        is_full: false,
                        delta_x: 0,
                        delta_y: 0,
                        delta_z: 0,
                    };
                }
            }
        }
    }

    pub fn add_rough_sphere(&mut self, center_x: f32, center_y: f32, center_z: f32, radius: f32) {
        let radius_squared = radius * radius;

        let x_min = ((center_x - radius).floor() as usize).max(0);
        let x_max = ((center_x + radius).ceil() as usize).min(self.size_x);
        let y_min = ((center_y - radius).floor() as usize).max(0);
        let y_max = ((center_y + radius).ceil() as usize).min(self.size_y);
        let z_min = ((center_z - radius).floor() as usize).max(0);
        let z_max = ((center_z + radius).ceil() as usize).min(self.size_z);

        for x in x_min..x_max {
            for y in y_min..y_max {
                for z in z_min..z_max {
                    let rx: f32 = (x as f32) - center_x;
                    let ry: f32 = (y as f32) - center_y;
                    let rz: f32 = (z as f32) - center_z;

                    let distance_squared: f32 = rx * rx + ry * ry + rz * rz;

                    if distance_squared < radius_squared {
                        self.data[x][y][z] = Corner {
                            is_full: true,
                            delta_x: 255,
                            delta_y: 255,
                            delta_z: 255,
                        };
                    }
                }
            }
        }
    }

    pub fn del_rough_sphere(&mut self, center_x: f32, center_y: f32, center_z: f32, radius: f32) {
        let radius_squared = radius * radius;

        let x_min = ((center_x - radius).floor() as usize).max(0);
        let x_max = ((center_x + radius).ceil() as usize).min(self.size_x);
        let y_min = ((center_y - radius).floor() as usize).max(0);
        let y_max = ((center_y + radius).ceil() as usize).min(self.size_y);
        let z_min = ((center_z - radius).floor() as usize).max(0);
        let z_max = ((center_z + radius).ceil() as usize).min(self.size_z);

        for x in x_min..x_max {
            for y in y_min..y_max {
                for z in z_min..z_max {
                    let rx: f32 = (x as f32) - center_x;
                    let ry: f32 = (y as f32) - center_y;
                    let rz: f32 = (z as f32) - center_z;

                    let distance_squared: f32 = rx * rx + ry * ry + rz * rz;

                    if distance_squared < radius_squared {
                        self.data[x][y][z] = Corner {
                            is_full: false,
                            delta_x: 0,
                            delta_y: 0,
                            delta_z: 0,
                        };
                    }
                }
            }
        }
    }

    pub fn add_perfect_sphere(&mut self, center_x: f32, center_y: f32, center_z: f32, radius: f32) {
        let radius_squared = radius * radius;

        let x_min = ((center_x - radius).floor() as usize).max(0);
        let x_max = ((center_x + radius).ceil() as usize).min(self.size_x);
        let y_min = ((center_y - radius).floor() as usize).max(0);
        let y_max = ((center_y + radius).ceil() as usize).min(self.size_y);
        let z_min = ((center_z - radius).floor() as usize).max(0);
        let z_max = ((center_z + radius).ceil() as usize).min(self.size_z);

        for x in x_min..x_max {
            for y in y_min..y_max {
                for z in z_min..z_max {
                    let rx: f32 = (x as f32) - center_x;
                    let ry: f32 = (y as f32) - center_y;
                    let rz: f32 = (z as f32) - center_z;

                    let distance_squared: f32 = rx * rx + ry * ry + rz * rz;

                    if distance_squared < radius_squared {
                        let delta_x =
                            self.get_perfect_sphere_delta(radius, center_x, x as f32, ry, rz);
                        let delta_y =
                            self.get_perfect_sphere_delta(radius, center_y, y as f32, rx, rz);
                        let delta_z =
                            self.get_perfect_sphere_delta(radius, center_z, z as f32, rx, ry);

                        self.data[x][y][z] = Corner {
                            is_full: true,
                            delta_x: delta_x,
                            delta_y: delta_y,
                            delta_z: delta_z,
                        };
                    }
                }
            }
        }
    }

    pub fn del_perfect_sphere(&mut self, center_x: f32, center_y: f32, center_z: f32, radius: f32) {
        let radius_squared = radius * radius;

        let x_min = ((center_x - radius).floor() as usize).max(0);
        let x_max = ((center_x + radius).ceil() as usize).min(self.size_x);
        let y_min = ((center_y - radius).floor() as usize).max(0);
        let y_max = ((center_y + radius).ceil() as usize).min(self.size_y);
        let z_min = ((center_z - radius).floor() as usize).max(0);
        let z_max = ((center_z + radius).ceil() as usize).min(self.size_z);

        for x in x_min..x_max {
            for y in y_min..y_max {
                for z in z_min..z_max {
                    let rx: f32 = (x as f32) - center_x;
                    let ry: f32 = (y as f32) - center_y;
                    let rz: f32 = (z as f32) - center_z;

                    let distance_squared: f32 = rx * rx + ry * ry + rz * rz;

                    if distance_squared < radius_squared {
                        self.data[x][y][z] = Corner {
                            is_full: false,
                            delta_x: 0,
                            delta_y: 0,
                            delta_z: 0,
                        };

                        let delta_x =
                            self.get_perfect_sphere_delta(radius, center_x, x as f32, ry, rz);
                        let delta_y =
                            self.get_perfect_sphere_delta(radius, center_y, y as f32, rx, rz);
                        let delta_z =
                            self.get_perfect_sphere_delta(radius, center_z, z as f32, rx, ry);

                        let new_delta_x = 255 - delta_x;
                        let new_delta_y = 255 - delta_y;
                        let new_delta_z = 255 - delta_z;

                        if 0 < x
                            && self.data[x - 1][y][z].is_full
                            && self.data[x - 1][y][z].delta_x > new_delta_x
                        {
                            self.data[x - 1][y][z].delta_x = new_delta_x;
                        }

                        if x < self.size_x - 1
                            && self.data[x + 1][y][z].is_full
                            && self.data[x + 1][y][z].delta_x > new_delta_x
                        {
                            self.data[x + 1][y][z].delta_x = new_delta_x;
                        }

                        if 0 < y
                            && self.data[x][y - 1][z].is_full
                            && self.data[x][y - 1][z].delta_y > new_delta_y
                        {
                            self.data[x][y - 1][z].delta_y = new_delta_y;
                        }

                        if y < self.size_y - 1
                            && self.data[x][y + 1][z].is_full
                            && self.data[x][y + 1][z].delta_y > new_delta_y
                        {
                            self.data[x][y + 1][z].delta_y = new_delta_y;
                        }

                        if 0 < z
                            && self.data[x][y][z - 1].is_full
                            && self.data[x][y][z - 1].delta_z > new_delta_z
                        {
                            self.data[x][y][z - 1].delta_z = new_delta_z;
                        }

                        if z < self.size_z - 1
                            && self.data[x][y][z + 1].is_full
                            && self.data[x][y][z + 1].delta_z > new_delta_z
                        {
                            self.data[x][y][z + 1].delta_z = new_delta_z;
                        }
                    }
                }
            }
        }
    }

    fn get_perfect_sphere_delta(
        &self,
        radius: f32,
        dimension0_center: f32,
        dimension0_absolute: f32,
        dimension1_relative: f32,
        dimension2_relative: f32,
    ) -> u8 {
        let cut = (radius * radius
            - dimension1_relative * dimension1_relative
            - dimension2_relative * dimension2_relative)
            .sqrt();

        let cut_l = dimension0_center - cut;
        let cut_u = dimension0_center + cut;

        let delta_l = dimension0_absolute - cut_l;
        let delta_u = cut_u - dimension0_absolute;

        let mut delta = 1.0_f32;

        if delta_l < delta {
            delta = delta_l;
        }

        if delta_u < delta {
            delta = delta_u;
        }

        (delta * 255.0_f32).round() as u8
    }

    pub fn add_ground_2d_perlin(&mut self, rngs: &Vec<Perlin2D>, center: (f64, f64, f64)) {
        let rngs_count = rngs.len();

        let half_size_x = (self.size_x as f64 - 1_f64) / 2_f64;
        let half_size_y = (self.size_y as f64 - 1_f64) / 2_f64;
        let half_size_z = (self.size_z as f64 - 1_f64) / 2_f64;

        let perlins: Vec<Perlin> = rngs
            .iter()
            .map(|p| Perlin::new().set_seed(p.seed))
            .collect();

        let mut heights = vec![vec![0_f64; self.size_y]; self.size_x];

        for x in 0..self.size_x {
            for y in 0..self.size_y {
                heights[x][y] = half_size_z;

                for p in 0..rngs_count {
                    let rng = &rngs[p];

                    let x_perl = rng.scale.0 * (rng.offset.0 + center.0 + x as f64 - half_size_x);
                    let y_perl = rng.scale.1 * (rng.offset.1 + center.1 + y as f64 - half_size_y);

                    let z_perl = rng.scale.2
                        * perlins[p]
                            .get([x_perl, y_perl])
                            .max(rng.clamp.0)
                            .min(rng.clamp.1);

                    heights[x][y] += rng.offset.2 + z_perl;
                }
            }
        }

        for x in 0..self.size_x {
            for y in 0..self.size_y {
                for z in 0..self.size_z {
                    let z_f64 = z as f64;

                    if z_f64 <= heights[x][y] {
                        let h = heights[x][y] - z_f64;
                        let mut h_x_u = 0_f64;
                        let mut h_x_l = 0_f64;
                        let mut h_y_u = 0_f64;
                        let mut h_y_l = 0_f64;

                        if 0 < x {
                            h_x_u = heights[x][y] - heights[x - 1][y];
                        }

                        if x < self.size_x - 1 {
                            h_x_l = heights[x][y] - heights[x + 1][y];
                        }

                        if 0 < y {
                            h_y_u = heights[x][y] - heights[x][y - 1];
                        }

                        if y < self.size_y - 1 {
                            h_y_l = heights[x][y] - heights[x][y + 1];
                        }

                        let mut delta_x_u8 = 0;
                        let mut delta_y_u8 = 0;
                        let mut delta_z_f64 = h * 255_f64;

                        if h > 0_f64 {
                            if h_x_l > 0_f64 {
                                let val = (255_f64 * h / h_x_l) as u8;
                                if delta_x_u8 == 0 || val < delta_x_u8 {
                                    delta_x_u8 = val;
                                }
                            }
                            if h_x_u > 0_f64 {
                                let val = (255_f64 * h / h_x_u) as u8;
                                if delta_x_u8 == 0 || val < delta_x_u8 {
                                    delta_x_u8 = val;
                                }
                            }
                            if h_y_l > 0_f64 {
                                let val = (255_f64 * h / h_y_l) as u8;
                                if delta_y_u8 == 0 || val < delta_y_u8 {
                                    delta_y_u8 = val;
                                }
                            }
                            if h_y_u > 0_f64 {
                                let val = (255_f64 * h / h_y_u) as u8;
                                if delta_y_u8 == 0 || val < delta_y_u8 {
                                    delta_y_u8 = val;
                                }
                            }
                        }

                        if delta_z_f64 > 255_f64 {
                            delta_z_f64 = 255_f64;
                        }

                        // println!(
                        //     "x:{}, y:{}, z:{}, h:{}, delta_x_u8:{}, delta_y_u8:{}, delta_z_f64:{}",
                        //     x, y, z, h, delta_x_u8, delta_y_u8, delta_z_f64
                        // );

                        // println!(
                        //     "h_x_l:{}, h_x_u:{}, h_y_l:{}, h_y_u:{}",
                        //     h_x_l, h_x_u, h_y_l, h_y_u,
                        // );

                        self.data[x][y][z] = Corner {
                            is_full: true,
                            delta_x: delta_x_u8,
                            delta_y: delta_y_u8,
                            delta_z: delta_z_f64 as u8,
                        };
                    }
                }
            }
        }
    }
}
