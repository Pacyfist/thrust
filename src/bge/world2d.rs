use pyo3::prelude::*;
use pyo3::types::{IntoPyDict, PyList};

use std::cmp::min;
use std::time::Instant;

use crate::logic::maps::Maps;
use crate::logic::mesh::Mesh;

use crate::dto::perlin2d::Perlin2D;
use crate::dto::world_chunk::WorldChunkDTO;

const ALLOW_OBJECT_DELETION_AT_RUNTIME: bool = true;
//UPBGE currently doesn't delete any objects at runtime. It only hides them. So why even bother deleting.

#[pyclass]
pub struct World2D {
    name: String,
    chunk_size: (usize, usize, usize),
    rngs: Vec<Perlin2D>,
    maps: Maps,
}

#[pymethods]
impl World2D {
    #[new]
    fn new(name: String, chunk_size: (usize, usize, usize)) -> Self {
        World2D {
            name: name,
            chunk_size: chunk_size,
            rngs: Vec::new(),
            maps: Maps::new(),
        }
    }

    pub fn add_perlin_noise(
        &mut self,
        seed: u32,
        offset: (f64, f64, f64),
        scale: (f64, f64, f64),
        clamp: (f64, f64),
    ) {
        let perlin = Perlin2D {
            seed: seed,
            offset: offset,
            scale: scale,
            clamp: clamp,
        };

        self.rngs.push(perlin);
    }

    pub fn bge_initialize(&mut self, point: (f32, f32), radius: usize) -> PyResult<()> {
        let now = Instant::now();

        let result = self.update(point, radius, usize::MAX);

        println!(
            "World initialised in {}s",
            (now.elapsed().as_millis() as f64) / 1000_f64,
        );

        result
    }

    pub fn update(&mut self, point: (f32, f32), radius: usize, limit: usize) -> PyResult<()> {
        Python::with_gil(|py| -> PyResult<()> {
            let now = Instant::now();

            let collection = self.set_collection(&py, &self.name)?;

            let existing_chunks = self.get_existing_chunks(collection)?;
            let required_chunks = self.get_required_chunks(point, radius)?;

            let chunks_to_add = self.get_chunks_to_add(&existing_chunks, &required_chunks, point);
            let chunks_to_del = self.get_chunks_to_del(&existing_chunks, &required_chunks, point);

            let bpy = py.import("bpy")?;
            let bge = py.import("bge")?;

            let mut offset = self.move_objects(&py, &chunks_to_add, &chunks_to_del, limit)?;

            offset = self.add_objects(
                &py,
                Some(bpy),
                Some(bge),
                &chunks_to_add,
                &collection,
                offset,
                limit,
            )?;

            if ALLOW_OBJECT_DELETION_AT_RUNTIME {
                offset = self.del_objects(
                    &py,
                    Some(bpy),
                    Some(bge),
                    &chunks_to_del,
                    &collection,
                    offset,
                    limit,
                )?;
            }

            if 0 < offset {
                println!(
                    "World Updated in {}s - Operations:{}",
                    (now.elapsed().as_millis() as f64) / 1000_f64,
                    offset
                );
            }

            Ok(())
        })
    }

    pub fn bpy_initialize(&mut self, point: (f32, f32), radius: usize) -> PyResult<()> {
        Python::with_gil(|py| -> PyResult<()> {
            let now = Instant::now();

            let collection = self.set_collection(&py, &self.name)?;

            let existing_chunks = self.get_existing_chunks(collection)?;
            let required_chunks = self.get_required_chunks(point, radius)?;

            let chunks_to_add = self.get_chunks_to_add(&Vec::new(), &required_chunks, point);
            let chunks_to_del = self.get_chunks_to_del(&existing_chunks, &Vec::new(), point);

            let bpy = py.import("bpy")?;

            self.del_objects(
                &py,
                Some(bpy),
                None,
                &chunks_to_del,
                &collection,
                0,
                usize::MAX,
            )?;

            self.add_objects(
                &py,
                Some(bpy),
                None,
                &chunks_to_add,
                &collection,
                0,
                usize::MAX,
            )?;

            println!(
                "World Initialised in {}s",
                (now.elapsed().as_millis() as f64) / 1000_f64,
            );

            Ok(())
        })
    }
}

impl World2D {}

impl World2D {
    fn set_collection<'py>(&self, py: &'py Python, name: &String) -> PyResult<&'py PyAny> {
        let collections = py.import("bpy")?.get("data")?.getattr("collections")?;

        let mut collection = collections.call_method1("get", (name,))?;

        if collection.is_none() {
            collection = collections.call_method1("new", (name,))?;

            py.import("bpy")?
                .get("context")?
                .getattr("collection")?
                .getattr("children")?
                .call_method1("link", (collection,))?;
        }

        Ok(collection)
    }

    fn get_chunk_name(&self, x: isize, y: isize) -> String {
        format!("{}.({}).({})", self.name, x, y)
    }

    fn get_existing_chunks(&self, collection: &PyAny) -> PyResult<Vec<WorldChunkDTO>> {
        let objects = collection.getattr("objects")?;
        let objects_iterator = objects.iter()?;

        let mut results: Vec<WorldChunkDTO> = vec![];

        print!("Chunks - ");

        for iter in objects_iterator {
            let object = iter.unwrap();
            let object_location = object.getattr("location")?;

            let name: String = object.getattr("name")?.extract()?;

            let x: f32 = object_location.getattr("x")?.extract()?;
            let y: f32 = object_location.getattr("y")?.extract()?;
            let z: f32 = object_location.getattr("z")?.extract()?;

            print!("{}, ",&name);

            let world_chunk_dto = WorldChunkDTO::new(
                name,
                (x.round() as isize, y.round() as isize, z.round() as isize),
            );

            println!("");

            results.push(world_chunk_dto);
        }

        Ok(results)
    }

    fn get_required_chunks<'py>(
        &self,
        point: (f32, f32),
        radius: usize,
    ) -> PyResult<Vec<WorldChunkDTO>> {
        let r = radius as isize;

        let center_x =
            (point.0 / self.chunk_size.0 as f32).round() as isize * self.chunk_size.0 as isize;
        let center_y =
            (point.1 / self.chunk_size.1 as f32).round() as isize * self.chunk_size.1 as isize;

        let delta_x = (r / self.chunk_size.0 as isize) * (self.chunk_size.0 as isize);
        let delta_y = (r / self.chunk_size.1 as isize) * (self.chunk_size.1 as isize);

        let mut results: Vec<WorldChunkDTO> = vec![];

        for x in (center_x - delta_x..center_x + delta_x + 1).step_by(self.chunk_size.0) {
            for y in (center_y - delta_y..center_y + delta_y + 1).step_by(self.chunk_size.1) {
                if (center_x - x) * (center_x - x) + (center_y - y) * (center_y - y) <= r * r {
                    let name = self.get_chunk_name(x, y);

                    let world_chunk_dto = WorldChunkDTO::new(name, (x, y, 0));

                    results.push(world_chunk_dto);
                }
            }
        }

        Ok(results)
    }

    fn sort_object_names(&self, names: &mut Vec<WorldChunkDTO>, point: (f32, f32), desc: bool) {
        let point_x = point.0 as isize;
        let point_y = point.1 as isize;

        names.sort_by_key(|name| {
            (point_x - name.location.0) * (point_x - name.location.0)
                + (point_y - name.location.1) * (point_y - name.location.1)
        });

        if desc {
            names.reverse();
        }
    }

    fn get_chunks_to_add(
        &self,
        existing: &Vec<WorldChunkDTO>,
        required: &Vec<WorldChunkDTO>,
        point: (f32, f32),
    ) -> Vec<WorldChunkDTO> {
        let mut result: Vec<WorldChunkDTO> = Vec::new();

        for rq in required {
            let mut found = false;

            for ex in existing {
                if rq.name == ex.name {
                    found = true;
                }
            }

            if !found {
                let r: WorldChunkDTO = rq.copy();
                result.push(r);
            }
        }

        self.sort_object_names(&mut result, point.clone(), false);

        result
    }

    fn get_chunks_to_del(
        &self,
        existing: &Vec<WorldChunkDTO>,
        required: &Vec<WorldChunkDTO>,
        point: (f32, f32),
    ) -> Vec<WorldChunkDTO> {
        let mut result: Vec<WorldChunkDTO> = Vec::new();

        for ex in existing {
            let mut found = false;

            for rq in required {
                if rq.name == ex.name {
                    found = true;
                }
            }

            if !found {
                let r: WorldChunkDTO = ex.copy();
                result.push(r);
            }
        }

        self.sort_object_names(&mut result, point.clone(), true);

        result
    }

    fn move_objects<'py>(
        &mut self,
        py: &'py Python,
        chunks_to_add: &Vec<WorldChunkDTO>,
        chunks_to_del: &Vec<WorldChunkDTO>,
        limit: usize,
    ) -> PyResult<usize> {
        let chunks_count = min(chunks_to_add.len(), chunks_to_del.len());
        let count = min(limit, chunks_count);

        if 0 < count {
            let bge_scene = py
                .import("bge")?
                .get("logic")?
                .call_method0("getCurrentScene")?;

            let bge_objects = bge_scene.getattr("objects")?;

            let bpy_context_view_layer = py.import("bpy")?.get("context")?.getattr("view_layer")?;

            for i in 0..count {
                let chunk_to_add = &chunks_to_add[i];
                let chunk_to_del = &chunks_to_del[i];

                let bge_object = bge_objects.call_method1("get", (&chunk_to_del.name,))?;
                let bpy_object = bge_object.getattr("blenderObject")?;

                bge_object
                    .getattr("position")?
                    .setattr("x", chunk_to_add.location.0)?;
                bpy_object
                    .getattr("location")?
                    .setattr("x", chunk_to_add.location.0)?;

                bge_object
                    .getattr("position")?
                    .setattr("y", chunk_to_add.location.1)?;
                bpy_object
                    .getattr("location")?
                    .setattr("y", chunk_to_add.location.1)?;

                //bge_object.setattr("name", &chunk_to_add.name)?;
                bpy_object.setattr("name", &chunk_to_add.name)?;

                self.update_mesh(&py, bpy_object)?;

                bpy_context_view_layer.call_method0("update")?;
                bge_object
                    .call_method1("reinstancePhysicsMesh", (py.None(), py.None(), false, true))?;
            }
        }

        Ok(count)
    }

    fn add_objects<'py>(
        &mut self,
        py: &'py Python,
        bpy: Option<&PyModule>,
        bge: Option<&PyModule>,
        chunks: &Vec<WorldChunkDTO>,
        collection: &'py PyAny,
        offset: usize,
        limit: usize,
    ) -> PyResult<usize> {
        let count = min(limit, chunks.len());

        if offset < count {
            let bpy_data = bpy.unwrap().get("data")?;

            let mut objects: Vec<&PyAny> = Vec::new();

            for chunk in &chunks[offset..count] {
                let object = self.add_object(&py, bpy_data, chunk)?;

                collection
                    .getattr("objects")?
                    .call_method1("link", (object,))?;

                self.update_mesh(&py, &object)?;

                objects.push(object);
            }

            if !bge.is_none() {
                let bge_scene = bge.unwrap().get("logic")?.call_method0("getCurrentScene")?;

                bge_scene.call_method1("convertBlenderObjectsList", (objects, false))?;
            }
        }

        Ok(count)
    }

    fn add_object<'py>(
        &self,
        py: &'py Python,
        bpy_data: &'py PyAny,
        chunk: &WorldChunkDTO,
    ) -> PyResult<&'py PyAny> {
        let name = &chunk.name;
        let x = chunk.location.0;
        let y = chunk.location.1;

        let meshes = bpy_data.getattr("meshes")?;
        let mesh = meshes.call_method1("new", (name,))?;

        let objects = bpy_data.getattr("objects")?;
        let object = objects.call_method(
            "new",
            (name,),
            Some(vec![("object_data", mesh)].into_py_dict(*py)),
        )?;

        let location = object.getattr("location")?;
        location.setattr("x", x)?;
        location.setattr("y", y)?;

        Ok(object)
    }

    fn del_objects<'py>(
        &mut self,
        py: &'py Python,
        bpy: Option<&PyModule>,
        bge: Option<&PyModule>,
        chunks: &Vec<WorldChunkDTO>,
        collection: &'py PyAny,
        offset: usize,
        limit: usize,
    ) -> PyResult<usize> {
        let count = min(limit, chunks.len());

        if offset < count {
            let bpy_data = bpy.unwrap().get("data")?;
            let bpy_objects = bpy_data.getattr("objects")?;

            let mut bge_objects = None;

            if !bge.is_none() {
                let bge_scene = bge.unwrap().get("logic")?.call_method0("getCurrentScene")?;

                bge_objects = Some(bge_scene.getattr("objects")?);
            }

            for chunk in &chunks[offset..count] {
                let bpy_object = bpy_objects.get_item(&chunk.name)?;

                collection
                    .getattr("objects")?
                    .call_method1("unlink", (bpy_object,))?;

                if !bge_objects.is_none() {
                    let bge_object = bge_objects.unwrap().call_method1("get", (&chunk.name,))?;

                    bge_object.call_method0("endObject")?;

                    println!("endObject - {}", bge_object);
                }

                bpy_objects.call_method(
                    "remove",
                    (bpy_object,),
                    Some(vec![("do_unlink", true)].into_py_dict(*py)),
                )?;
            }
        }

        Ok(count)
    }

    fn update_mesh(&mut self, py: &Python, object: &PyAny) -> PyResult<()> {
        let name: String = object.getattr("name")?.extract()?;

        let location = object.getattr("location")?;
        let x: f64 = location.getattr("x")?.extract()?;
        let y: f64 = location.getattr("y")?.extract()?;
        let z: f64 = location.getattr("z")?.extract()?;

        if !self.maps.exists(&name) {
            self.maps.create(
                &name,
                self.chunk_size.0,
                self.chunk_size.1,
                self.chunk_size.2,
            );

            match self.maps.get(&name) {
                Some(map) => {
                    map.add_ground_2d_perlin(&self.rngs, (x, y, z));
                }
                None => {}
            }
        }

        match self.maps.get(&name) {
            Some(map) => {
                let data = object.getattr("data")?;

                data.call_method0("clear_geometry")?;

                let mesh = Mesh::new(&map);

                let verts: Vec<(f32, f32, f32)> = mesh.verts;
                let edges: Vec<(usize, usize)> = Vec::new();
                let faces: Vec<(usize, usize, usize)> = mesh.faces;

                let pyverts: &PyList = PyList::new(*py, &verts);
                let pyedges: &PyList = PyList::new(*py, &edges);
                let pyfaces: &PyList = PyList::new(*py, &faces);

                data.call_method1("from_pydata", (pyverts, pyedges, pyfaces))?;
            }
            None => {}
        }

        Ok(())
    }
}
